import Boom from "@hapi/boom";

export default (err, req, res, next) => {
  if (err && err.error) {
    return res.status(400).json({
      type: err.type,
      message: err.error.toString(),
    });
  }
  if (Boom.isBoom(err)) {
    const payload = {};
    if (err.data) {
      payload.errorDetail = err.data;
    }
    payload.message = err.output.payload.message;
    return res.status(err.output.statusCode).send(payload);
  }
  res.status(500).json({
    message: err.message,
    errorName: err.name,
  });
};
