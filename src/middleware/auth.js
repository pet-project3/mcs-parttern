import { getOr } from "lodash/fp";
import { verifyToken } from "./../utils";
import userService from "../services/user";
import Boom from "@hapi/boom";
import { isEmpty } from "lodash";

const isAuthenMiddleware = async (req, res, next) => {
  try {
    const token = getOr(null, "authorization")(req.headers);
    if (!token) {
      throw Boom.unauthorized("User un-authenticated");
    }
    const decoded = await verifyToken(token);
    const user_id = getOr(null, "id")(decoded);
    const { err, result } = userService.findById(user_id);
    if (err || isEmpty(result)) {
      throw Boom.unauthorized("User un-authenticated");
    }
    next();
  } catch (err) {
    next(err);
  }
};

export default isAuthenMiddleware;
