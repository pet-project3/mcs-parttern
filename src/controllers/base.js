import Boom from "@hapi/boom";

const getAll = (service) => (req, res, next) => {
  try {
    const { err, result } = service.findAll();
    if (err) {
      throw Boom.badRequest(err);
    }
    res.send(result);
  } catch (err) {
    next(err);
  }
};

const getById = (service) => (req, res, next) => {
  try {
    const { id } = req.params;
    const { err, result } = service.findById(id);
    if (err) {
      throw Boom.badRequest(err);
    }
    res.send(result);
  } catch (err) {
    next(err);
  }
};

export const baseCtl = (service) => {
  return {
    getAll: getAll(service),
    getById: getById(service),
  };
};
