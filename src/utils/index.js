import { verify } from "jsonwebtoken";
import fs from "fs";
import path from 'path'

const publicKey = fs.readFileSync(path.resolve("./src/utils/public.key"), "utf-8");

const verifyOptions = {
  expiresIn: "30d",
  algorithm: ["RS256"],
};

export const ensureENV = (key) => {
  if (!process.env[key]) {
    console.log(`Error: Please specify ${key} in environment`);
    process.exit(1);
  }
};

export const verifyToken = (token) => {
  try {
    const decoded = verify(token, publicKey,verifyOptions);
    return decoded;
  } catch (err) {
    console.log({err})
    return {};
  }
};
