import Joi from "joi";

const getByIdSchema = Joi.object({
  id: Joi.string()
    .regex(/^[0-9]*$/)
    .required(),
});

const saveSchema = Joi.object({
  id: Joi.string(),
  name: Joi.string(),
});

export const validateSchema = {
  getByIdSchema,
  saveSchema,
};
