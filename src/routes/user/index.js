import express from "express";
import validateMiddleware, {
  VALIDATE_TYPE_MAP,
} from "./../../middleware/validate";
import { userCtl } from "./../../controllers/user";
import { validateSchema } from "./validate";

const router = express.Router();

router.get("/getAll", userCtl.getAll);
router.get(
  "/:id",
  validateMiddleware(validateSchema.getByIdSchema, VALIDATE_TYPE_MAP.PARAMS),
  userCtl.getById
);
router.post("/", validateMiddleware(validateSchema.saveSchema), userCtl.save);

export default router;
