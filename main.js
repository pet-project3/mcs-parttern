import express from "express";
import userRoute from "./src/routes/user";
import petRoute from "./src/routes/pet";
import handleError from "./src/middleware/handleError";
import { ensureENV } from "./src/utils";
import isAuthenMiddleware from "./src/middleware/auth"

const PORT = process.env.PORT || 3000;

const app = express();

app.get("/", (req, res) => {
  res.send("status ok");
});

app.use("/user",isAuthenMiddleware, userRoute);

app.use("/pet",isAuthenMiddleware, petRoute);

app.use(handleError);

app.listen(PORT, () => {
  console.log(`server listen on port ${PORT}`);
});

export default app;
