import server from "./../../main";
import chai from "chai";
import chaiHttp from "chai-http";

const expect = chai.expect;

chai.use(chaiHttp);
const token =
  "eyJraWQiOiJkOTljZjg0Ny1lNDZkLTRjZDYtYTMzMi1hMTY2OTU5OTRkMTQiLCJhbGciOiJSUzI1NiJ9.ew0KICAic3ViIjogIjEyMzQ1Njc4OTAiLA0KICAibmFtZSI6ICJBbmlzaCBOYXRoIiwNCiAgImlhdCI6IDE1MTYyMzkwMjIsDQogICJpZCI6ICIxIg0KfQ.oSaE5FVQHv6DygkaJFPgZJ2p97D0Ho5k1YbRV8iX8DfvtMMnMEIJ0dcU_i0sRswHdlz2JrSPqDPAQhrZ1ELfUKXGyyaGoPAU-QYKbPjNWa4wwVyu6IRpTSQfLvg9zI23YJ6bhYZu4JJm8U9HKZig8pE8xzr3Z1Qea4mxzcQ98I3LsVcdRMsLwjd6M27RTdP3zus1iQu6l2LEKsDwetLTGHApp6h5KbcDl98Yr1fVj6-rMnfM12LN0miT7iliZm7ZPmflyRy9Tu8R6SlMKHas_v2xZqJ6rXoWzl0Ptt0GvbF_fQ9P0fGyFecR1Ek0t4X3BbmNuqe8OVlaRdIXTFo9zA";

describe("user api", () => {
  it("can return 401 by not authen", (done) => {
    chai
      .request(server)
      .get("/user/getAll")
      .set("authorization", "1234")
      .end((err, res) => {
        expect(res).to.have.status(401);
      });
    done();
  });
  it("can get all user", (done) => {
    chai
      .request(server)
      .get("/user/getAll")
      .set("authorization", token)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).equal(
          JSON.stringify([
            { id: "1", username: "user1", password: "1234" },
            { id: "2", username: "user2", password: "4567" },
            { id: "3", username: "user3", password: "6789" },
          ])
        );
      });
    done();
  });
  it("can get user by id", (done) => {
    chai
      .request(server)
      .get("/user/1")
      .set("authorization", token)
      .end((req, res) => {
        expect(res).to.have.status(200);
        expect(res.text).equal(
          JSON.stringify({ id: "1", username: "user1", password: "1234" })
        );
      });
    done();
  });
  it("can return 400 by wrong request", (done) => {
    chai
      .request(server)
      .get("/user/a")
      .set("authorization", token)
      .end((req, res) => {
        expect(res).to.have.status(400);
      });
    done();
  });
});
